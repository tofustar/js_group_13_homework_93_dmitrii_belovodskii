const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');
const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require("./models/Track");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [Cent, Justin, Eminem] = await Artist.create({
    name: '50cent',
    image: '50-cent.jpg',
    info: 'Very popular hip hop artist in 2000'
  },{
    name: 'Justin Bieber',
    image: 'justin.jpg',
    info: 'Justin Bieber was born in Canada'
  }, {
    name: 'Eminem',
    image: 'eminem.jpg',
    info: 'Eminem most popular "white" hip hop artist in the world'
  });

  const [GetRichorDieTryin, Curtis, MyWorld, TheMarshallMathersLP] = await Album.create({
    name: 'Get Rich or Die Tryin',
    artist: Cent,
    year: '2001',
    image: '50cent-first-album.JPG'
  },{
    name: 'Curtis',
    artist: Cent,
    year: '2007',
    image: '50cent-second-album.JPG.jpg'
  },{
    name: 'My World',
    artist: Justin,
    year: '2010',
    image: 'my-world.jpg'
  }, {
    name: 'The Marshall Mathers LP',
    artist: Eminem,
    year: '2000',
    image: 'eminem-album.jpg'
  });

  await Track.create({
    name: 'What Up Gangsta',
    album: GetRichorDieTryin,
    duration: '2:59'
  },{
    name: 'Patiently Waiting',
    album: GetRichorDieTryin,
    duration: '4:48'
  },{
    name: 'Many Men',
    album: GetRichorDieTryin,
    duration: '4:16'
  },{
    name: 'In da Club',
    album: GetRichorDieTryin,
    duration: '3:13'
  },{
    name: 'P.I.M.P.',
    album: GetRichorDieTryin,
    duration: '4:09'
  },{
    name: 'My Gun Go Off',
    album: Curtis,
    duration: '3:12'
  },{
    name: 'Man Down',
    album: Curtis,
    duration: '2:49'
  },{
    name: 'I Get Money',
    album: Curtis,
    duration: '3:43'
  },{
    name: 'Ayo Technology',
    album: Curtis,
    duration: '4:08'
  },{
    name: 'Follow My Lead',
    album: Curtis,
    duration: '3:17'
  },{
    name: 'Straight to the Bank',
    album: Curtis,
    duration: '3:10'
  },{
    name: 'One Time',
    album: MyWorld,
    duration: '3:35'
  },{
    name: 'Favorite Girl',
    album: MyWorld,
    duration: '4:16'
  },{
    name: 'Down to Earth',
    album: MyWorld,
    duration: '4:05'
  },{
    name: 'Bigger',
    album: MyWorld,
    duration: '3:17'
  },{
    name: 'One Less Lonely Girl',
    album: MyWorld,
    duration: '3:49'
  },{
    name: 'First Dance',
    album: MyWorld,
    duration: '3:42'
  },{
    name: 'Love Me',
    album: MyWorld,
    duration: '3:12'
  },{
    name: 'Kill You',
    album: TheMarshallMathersLP,
    duration: '4:24'
  },{
    name: 'Stan',
    album: TheMarshallMathersLP,
    duration: '6:44'
  },{
    name: 'Who Knew',
    album: TheMarshallMathersLP,
    duration: '3:47'
  },{
    name: 'The Way I Am',
    album: TheMarshallMathersLP,
    duration: '4:50'
  },{
    name: 'The Real Slim Shady',
    album: TheMarshallMathersLP,
    duration: '4:44'
  },{
    name: 'Remember Me?',
    album: TheMarshallMathersLP,
    duration: '3:38'
  },{
    name: 'Im back',
    album: TheMarshallMathersLP,
    duration: '5:10'
  },{
    name: 'Marshall Mathers',
    album: TheMarshallMathersLP,
    duration: '5:20'
  });

  await User.create({
    email: 'user@music.com',
    password: '12345',
    token: nanoid(),
    displayName: 'User',
    avatar: 'User_icon.png',
    role: 'user'
  },{
    email: 'admin@music.com',
    password: 'qwerty',
    token: nanoid(),
    displayName: 'Admin',
    avatar: 'Admin_icon.png',
    role: 'admin'
  })

  await mongoose.connection.close();
};

run().catch(e => console.error(e));