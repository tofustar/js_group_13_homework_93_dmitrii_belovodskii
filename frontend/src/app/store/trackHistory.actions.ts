import {createAction, props} from "@ngrx/store";
import {TrackHistory} from "../models/trackHistory.model";

export const addToTrackHRequest = createAction('[Track History] Add Request', props<{idTrack: string}>());
export const addToTrackHSuccess = createAction('[Track History] Add Success');
export const addToTrackHFailure = createAction('[Track History] Add Failure', props<{error:string}>());

export const fetchTrackHRequest = createAction('[Track History] Fetch Request');
export const fetchTrackHSuccess = createAction('[Track History] Fetch Success', props<{trackHistory: TrackHistory[]}>());
export const fetchTrackHFailure = createAction('[Track History] Fetch Failure', props<{error: string}>());