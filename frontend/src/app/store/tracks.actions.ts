import {createAction, props} from "@ngrx/store";
import {Track} from "../models/track.model";

export const fetchTracksRequest = createAction('[Tracks] Fetch Request');
export const fetchTracksSuccess = createAction('[Tracks] Fetch Success', props<{tracks: Track[]}>());
export const fetchTracksFailure = createAction('[Tracks] Fetch Request', props<{error: string}>());

export const fetchTracksByAlbumRequest = createAction('[TracksByAlbum] Fetch Request', props<{idAlbum: string}>());
export const fetchTracksByAlbumSuccess = createAction('[TracksByAlbum] Fetch Success', props<{tracks: Track[]}>());
export const fetchTracksByAlbumFailure = createAction('[TracksByAlbum] Fetch Failure', props<{error:string}>());

