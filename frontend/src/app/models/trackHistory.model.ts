import {Track} from "./track.model";

export class TrackHistory {
  constructor(
    public _id: string,
    public user: string,
    public track: Track,
    public datetime: string,
  ) {}
}
