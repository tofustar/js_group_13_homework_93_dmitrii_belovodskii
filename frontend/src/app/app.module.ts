import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from "@angular/flex-layout";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {LayoutComponent} from './ui/layout/layout.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {ArtistsComponent} from './pages/artists/artists.component';
import {MatButtonModule} from "@angular/material/button";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatCardModule} from "@angular/material/card";
import {AlbumsComponent} from './pages/albums/albums.component';
import {ImagePipe} from "./pipes/image.pipe";
import {RegisterComponent} from './pages/register/register.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {ValidateIdenticalDirective} from "./directives/validate-identical.directive";
import {FileInputComponent} from "./ui/file-input/file-input.component";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {LoginComponent} from './pages/login/login.component';
import {CenteredCardComponent} from './ui/centered-card/centered-card.component';
import {MatMenuModule} from "@angular/material/menu";
import {TracksComponent} from './pages/tracks/tracks.component';
import {TrackHistoryComponent} from './pages/track-history/track-history.component';
import {AppStoreModule} from "./app-store.module";
import {FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule} from "angularx-social-login";
import {environment} from "../environments/environment";

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.fbClientId, {
        scope: 'email,public_profile',
      })
    }
  ]
};

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    ArtistsComponent,
    AlbumsComponent,
    ImagePipe,
    RegisterComponent,
    ValidateIdenticalDirective,
    FileInputComponent,
    LoginComponent,
    CenteredCardComponent,
    TracksComponent,
    TrackHistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    FormsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatMenuModule,
    AppStoreModule,
    SocialLoginModule,
  ],
  providers: [
    {provide: 'SocialAuthServiceConfig', useValue: socialConfig},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
