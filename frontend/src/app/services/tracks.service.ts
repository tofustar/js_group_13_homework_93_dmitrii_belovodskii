import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ApiTrackData} from "../models/track.model";
import {environment as env} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class TracksService {
  constructor(private http: HttpClient) {}

  getTracks() {
    return this.http.get<ApiTrackData[]>(env.apiUrl + '/tracks');
  }

  getTracksByAlbum(albumId: string) {
    return this.http.get<ApiTrackData[]>(env.apiUrl + '/tracks?album=' + albumId);
  }
}