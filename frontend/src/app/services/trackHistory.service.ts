import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment as env} from "../../environments/environment";
import {TrackHistory} from "../models/trackHistory.model";

@Injectable({
  providedIn: 'root'
})

export class TrackHistoryService {
  constructor(private http: HttpClient) {}

  getTrackHistory(token: string) {
    return this.http.get<TrackHistory[]>(env.apiUrl + '/track_history', {
      headers: new HttpHeaders({'Authorization': token}),
    });
}

  addToTrackHistory(token: string, idTrack: string) {
    return this.http.post(env.apiUrl + '/track_history', {idTrack}, {
      headers: new HttpHeaders({'Authorization': token}),
    });
  }
}