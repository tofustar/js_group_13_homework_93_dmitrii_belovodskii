import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Observable, Subscription} from "rxjs";
import {LoginError, LoginUserData} from "../../models/user.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/types";
import {fbLoginUserRequest, loginUserRequest} from "../../store/users.actions";
import {FacebookLoginProvider, SocialAuthService, SocialUser} from "angularx-social-login";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | LoginError>;
  authStateSub!: Subscription;

  constructor(private store: Store<AppState>, private authService: SocialAuthService,) {
    this.loading = store.select(state => state.users.loginLoading);
    this.error = store.select(state => state.users.loginError);
  }

  onSubmit() {
    const userData: LoginUserData = this.form.value;
    this.store.dispatch(loginUserRequest({userData}));
  }

  fbLogin() {
    void this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  ngOnInit() {
    this.authStateSub = this.authService.authState.subscribe((user: SocialUser) => {
      const userData = {
        authToken: user.authToken,
        id: user.id,
        email: user.email,
        displayName: user.name,
        avatar: user.photoUrl,
      };

      this.store.dispatch(fbLoginUserRequest({userData}));
    });
  }

  ngOnDestroy() {
    this.authStateSub.unsubscribe();
  }

}
