import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/types";
import {TrackHistory} from "../../models/trackHistory.model";
import {fetchTrackHRequest} from "../../store/trackHistory.actions";

@Component({
  selector: 'app-track-history',
  templateUrl: './track-history.component.html',
  styleUrls: ['./track-history.component.sass']
})
export class TrackHistoryComponent implements OnInit {

  trackHistories: Observable<TrackHistory[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>) {
    this.trackHistories = store.select(state => state.trackHistories.trackHistory);
    this.loading = store.select(state => state.trackHistories.fetchLoading);
    this.error = store.select(state => state.trackHistories.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchTrackHRequest());

  }

}
